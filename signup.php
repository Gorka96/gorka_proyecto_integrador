<?php
/**
 * @title: Proyecto integrador Ev01 - Registro en el sistema.
 * @description:  Script PHP para almacenar un nuevo usuario en la base de datos
 *
 * @version    0.1
 *
 * @author ander_frago@cuatrovientos.org
 */

require_once 'header.php';

$error = $user = $pass = "";

if (isset($_POST['user'])) {

  // TODO Realiza la lectura de los campos del formulario en $user y $pass
  // HECHOOOO
  $user=$_POST['user'];
  $pass=$_POST['pass'];
  
  if ($user == "" || $pass == "") {
    $error = "Debes completar todos los campos<br><br>";
  }
  else {
    $result = queryMysql("SELECT * FROM usuarios WHERE nombre='$user'");

    if ($result->num_rows) {
      $error = "El usuario ya existe<br><br>";
    }
    else {
      queryMysql("INSERT INTO usuarios(nombre,contraseña) VALUES('$user', '$pass')");

      // TODO
      // Establecer el almacenamiento de usuario en una variable "user" almacenada en sesión
      // para que al pulsar sobre el menú de Acceder no se le vuelva a preguntar por usuario/contraseña
      // HECHOOO
      $_SESSION['user']=$user;
      $_SESSION['pass']=$pass;


      header('Location: login.php?');

    }
  }
}
?>

<div class="container">
    <form class="form-horizontal" role="form" method="POST" action="signup.php">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h2>Por favor registrate</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group has-danger">
                    <label class="sr-only" for="user">Usuario:</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i
                                    class="fa fa-at"></i></div>
                        <input type="text" name="user" class="form-control"
                               id="email"
                               placeholder="Nombre de Usuario" required
                               autofocus>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="sr-only" for="pass">Contraseña:</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i
                                    class="fa fa-key"></i></div>
                        <input type="password" name="pass"
                               class="form-control" id="password"
                               placeholder="Password" required>
                    </div>
                </div>
            </div>
           
        </div>
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                  <div class="form-control-feedback">
                      <span class="text-danger align-middle">
                     <?php
                      if (isset($error)){
                         echo $error;
                      }  
                     ?>
                      </span>
                  </div>
            </div>
        </div>
        <div class="row" style="padding-top: 1rem">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i> Acceder</button>
            </div>
        </div>
    </form>
</div>
